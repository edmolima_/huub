# Challenge Huub

In this project, I used a stack I'm most familiar with, which involves Nexjs and Typescript.

Tailwind was used for styling. Other tools to ensure a consistent pattern were used like eslint and pretty. In addition to a git hook to ensure that eslint and other tasks are run



## Run Project

for you to run this local project based on following the commands below

``
  yarn i
``

``
  yarn dev
``

## improvements

For this test some improvements to be made would certainly be:

- build private routes
- do all unit tests aimed at user behavior
- the inclusion of the details page