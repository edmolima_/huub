async function postLogin({ email, password }) {
  const res = await fetch(`https://api.brand.uat.thehuub.io/authenticate`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      email,
      password
    })
  })

  const { data } = await res.json()

  if (!data) {
    return {
      notFound: true,
    }
  }

  return {
    data
  }
}

export default postLogin