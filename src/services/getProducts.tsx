const BASE_URL = process.env.BASE_URL_API

async function getProducts({ token, page }) {

  const options = {
    headers: {
      "jwt": token
    }
  }

  const res = await fetch(`${BASE_URL}/products?page=${page}&page_size=10`, options)
  const products = await res.json()

  if (!products) {
    return {
      notFound: true,
    }
  }

  return products
}

export default getProducts