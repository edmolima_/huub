
import { useRouter } from 'next/router'
import { useEffect } from "react"

import Layout from "../templates/Layout"
import SideBar from "../components/Sidebar"
import LoginForm from "../components/LoginForm"
const IndexPage = () => {

  return (
    <Layout
      title="Huub - Login"
    >
      <main>
        <div className="flex w-full h-screen">
          <div className="grid grid-cols-1 w-1/4">
            <SideBar />
          </div>
          <div className="grid grid-cols-1 w-3/4">
            <LoginForm />
          </div>
        </div>
      </main>
    </Layout>
  )
}

export default IndexPage