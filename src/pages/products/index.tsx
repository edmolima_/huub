
import { withRouter } from 'next/router'
import usePagination from "../../hooks/usePagination"

import Layout from "../../templates/Layout"
import ProductFeed from "../../components/ProductFeed"
import getProducts from "../../services/getProducts"

const ProductPage = (props: any) => {

  const { products, router } = props
  const { pagginationHandler, isLoading } = usePagination({ router })
  return (
    <Layout
      title="Huub - Products"
    >
      <main>
        <ProductFeed products={products} paginationHandler={pagginationHandler} loading={isLoading} />
      </main>
    </Layout>
  )
}

export async function getServerSideProps({ query }) {
  const page = query.page || 1;
  const token = process.env.TOKEN
  const products = await getProducts({ token, page })

  return {
    props: {
      products
    },
  }
}

export default withRouter(ProductPage);