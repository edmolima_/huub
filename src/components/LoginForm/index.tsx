import { useForm } from 'react-hook-form'
import { useRouter } from 'next/router'
import postLogin from '../../services/postLogin';

function LoginForm() {
  const { register, handleSubmit } = useForm();
  const router = useRouter()

  const onSubmit = async ({ email, password }) => {
    try {
      const { data } = await postLogin({ email, password })

      if (data?.jwt) {
        localStorage.setItem('jwt', JSON.stringify(data.jwt))
        return router.push('/products')
      } else {
        alert('invalid credentials')
      }

    } catch (error) {
      return null
    }
  }

  return (
    <section className="p-20">
      <form onSubmit={handleSubmit(onSubmit)} className="mt-8 max-w-md">
        <h1 className="text-5xl py-7">Sign in to <span className="font-black">@huub</span></h1>
        <label className="block py-2" htmlFor="inputEmail">
          <span className="text-gray-700">Email address</span>
          <input {...register('email')} id="inputEmail" name="email" type="email" className="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-gray-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="john@example.com" />
        </label>
        <label className="block py-2" htmlFor="inputPassword">
          <span className="text-gray-700">Password</span>
          <input {...register('password')} id="inputPassword" name="password" type="password" className="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-gray-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" placeholder="" />
        </label>
        <label className="block py-5">
          <button className="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow" type="submit">
            Sign In
          </button>
        </label>
      </form>
    </section>
  )
}

export default LoginForm