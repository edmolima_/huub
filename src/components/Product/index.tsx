function ProductItem({ code, title }) {
  return (
    <div className="border rounded-lg">
      <div className="flex items-center space-x-4 p-4">
        <div className="flex items-center p-4 bg-gray-600 text-white rounded-lg">
          <svg className="fill-current text-emerald-600 inline-block h-6 w-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
            <path d="M18 9.87V20H2V9.87a4.25 4.25 0 0 0 3-.38V14h10V9.5a4.26 4.26 0 0 0 3 .37zM3 0h4l-.67 6.03A3.43 3.43 0 0 1 3 9C1.34 9 .42 7.73.95 6.15L3 0zm5 0h4l.7 6.3c.17 1.5-.91 2.7-2.42 2.7h-.56A2.38 2.38 0 0 1 7.3 6.3L8 0zm5 0h4l2.05 6.15C19.58 7.73 18.65 9 17 9a3.42 3.42 0 0 1-3.33-2.97L13 0z"></path>
          </svg>
        </div>
        <div className="flex-1">
          <p className="text-gray-500 font-semibold">code: {code}</p>
          <div className="flex items-baseline space-x-4">
            <h2 className="text-2xl font-semibold">{title}</h2>
          </div>
        </div>
      </div>
      <a href="#" className="block p-3 text-lg font-semibold bg-gray-50 text-gray-800 hover:bg-gray-100 cursor-pointer">
        View Detail
      </a>
    </div>
  )
}

export default ProductItem