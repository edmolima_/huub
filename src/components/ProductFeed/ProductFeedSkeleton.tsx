function ProductFeedSkeleton() {

  return (
    <div className='md:grid md:grid-cols-3 md:gap-4 space-y-4 md:space-y-0 mt-4'>
      {Array.from(Array(10).keys()).map((i) => (
        <div className="border rounded-lg" key={i}>
          <div className="flex items-center space-x-4 p-4">
            <div className="flex items-center p-4 bg-gray-600 text-white rounded-lg">
              <div className="rounded-full flex items-center p-3 bg-gray-600 text-white rounded-lg"></div>
            </div>
            <div className="flex-1">
              <div className="block p-2 text-lg font-semibold bg-gray-50 text-gray-800 hover:bg-gray-100">
                <div className="col-span-2 h-4 rounded-sm bg-gray-200 animate-pulse"></div>
              </div>
              <div className="block p-2 text-lg font-semibold bg-gray-50 text-gray-800 hover:bg-gray-100">
                <div className="col-span-2 h-4 rounded-sm bg-gray-200 animate-pulse"></div>
              </div>
            </div>
          </div>
          <div className="block p-3 text-lg font-semibold bg-gray-50 text-gray-800 hover:bg-gray-100">
            <div className="col-span-2 h-4 rounded-sm bg-gray-200 animate-pulse"></div>
          </div>
        </div>
      ))}
    </div>
  )
}

export default ProductFeedSkeleton