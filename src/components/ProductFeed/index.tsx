import Product from "../Product"
import Pagination from "../Pagination"
import ProductFeedSkeleton from './ProductFeedSkeleton'

function ProductFeed({ products, paginationHandler, loading }) {
  const { data, paginator } = products

  return (
    <section className="container h-screen mx-auto">
      <section className="flex flex-col mt-2 py-5 space-y-1 sm:flex-row sm:space-y-0 sm:items-center sm:space-x-4">
        <h3 className="text-2xl font-semibold text-gray-800 dark:text-gray-200">Product Catalog</h3>
      </section>
      <section className='flex flex-col'>
        {loading ? (
          <ProductFeedSkeleton />
        ) : (
          <section className="md:grid md:grid-cols-3 md:gap-4 space-y-4 md:space-y-0 mt-4">
            {data.map(({ id, name }) => (
              <Product key={id} code={id} title={name} />
            ))}
          </section>
        )}
        <section className="flex py-10 flex justify-center content-center">
          <Pagination
            paginator={paginator}
            paginationHandler={paginationHandler}
          />
        </section>
      </section>
    </section>
  )
}

export default ProductFeed
