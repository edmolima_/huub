import ReactPaginate from "react-paginate"

function Pagination({ paginator, paginationHandler }) {
  return (
    <ReactPaginate
      previousLabel={'previous'}
      nextLabel={'next'}
      breakLabel={'...'}
      breakClassName='relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700'
      containerClassName={"flex flex-row justify-between"}
      pageLinkClassName='bg-white border-gray-300 text-gray-500 hover:bg-gray-50 relative inline-flex items-center px-4 py-2 border text-sm font-medium'
      previousLinkClassName={"relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50"}
      nextLinkClassName='relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50'
      activeLinkClassName='z-10 bg-indigo-50 border-indigo-500 text-indigo-600'
      initialPage={paginator.page_number - 1}
      pageCount={paginator.pageCount}
      marginPagesDisplayed={2}
      pageRangeDisplayed={5}
      onPageChange={paginationHandler}
    />
  )
}

export default Pagination
