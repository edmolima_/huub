function SideBar() {
  return (
    <header className="bg-black">
      <section className="p-12">
        <h2 className="text-white text-6xl leading-tight">
          The logistics platform for <span className="text-white font-black">fashion brands</span>
        </h2>
      </section>
      <section className="px-12">
        <span className="text-white text-2xl font-thin">The service you envisioned. The growth we empower. All in one.</span>
      </section>
    </header>
  )
}

export default SideBar